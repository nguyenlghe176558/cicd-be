package com.kas.online_book_shop.enums;

public enum Role {
    USER,
    MARKETING,
    SELL,
    ADMIN
}
