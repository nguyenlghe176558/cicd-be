package com.kas.online_book_shop.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kas.online_book_shop.enums.Role;
import com.kas.online_book_shop.model.User;
import com.kas.online_book_shop.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
//@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001", "http://localhost", "http://74.235.148.227:3000", "http://74.235.148.227:3001"})
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;

    @GetMapping("")
    public ResponseEntity<List<User>> getUser() {
        var users = userService.getAllUsers();
        if (users.isEmpty())
            return ResponseEntity.noContent().build();
        else
            return ResponseEntity.ok(users);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        User user = userService.getUserById(id);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping()
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.updateUser(user));
    }

    @GetMapping("/role")
    public ResponseEntity<List<User>> getUserByRole(@RequestParam Role role) {
        var users = userService.getUserByRole(role);
        if (users.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(users);
        }

    }

    @GetMapping("/by-email/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @PutMapping("/set-account-state/{id}")
    public ResponseEntity<Void> setAccountState(
            @PathVariable Long id,
            @RequestParam String state) {
            userService.setAccountState(id, state);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/set-role/{id}")
    public ResponseEntity<Void> setRole(
            @PathVariable Long id,
            @RequestParam String role) {
            userService.setRole(id, role);
        return ResponseEntity.ok().build();
    }
}
